 
    function toggleTwoClasses(obj, class1, class2){
        if(obj.hasClass(class1) & !obj.hasClass(class2)){
            obj.removeClass(class1);
            obj.addClass(class2);
        }else if (obj.hasClass(class2) & !obj.hasClass(class1)){
            obj.removeClass(class2);
            obj.addClass(class1);
        }
    }

    function menu() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active').removeClass('icon-close-big').addClass('icon-icon-search-big');
            $('.search-block input[type="text"]').blur();
            
        } else {
            if($(window).width() >= 768){
                setTimeout(function () {
                    $('.search-block input[type="text"]').focus();
                }, 300);
            }
            $(this).addClass('active').removeClass('icon-icon-search-big').addClass('icon-close-big');
        }
        $('.mobile').toggleClass('active');
        $('.main-page').toggleClass('active');
        $('body').toggleClass('no-scroll');
    }

    $('.toggle-menu').on('click', function () {
        $('.search').trigger("click");
        toggleTwoClasses($('.search'), 'icon-icon-search-big', 'icon-icon-search-big-white');
    });

    $('.search').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active').removeClass('icon-close-big').addClass('icon-icon-search-big-white');
            $('.search-block input[type="text"]').blur();

            $('.to-top').css('width', 'calc(100% - 85px)');
            console.log($('.main-page.active').css('margin-left'));

            $('.toggle-menu').addClass('icon-toggle_tablet');
            $('.toggle-menu').removeClass('icon-menu-online-close');


        } else {
            if($(window).width() >= 768){
                setTimeout(function () {
                    $('.search-block input[type="text"]').focus();
                }, 300);
            }
            $(this).addClass('active').removeClass('icon-icon-search-big-white').removeClass('icon-icon-search-big').addClass('icon-close-big');

             $('.to-top').css('width', 'calc(100% - 360px)');
            console.log($('.main-page.active').css('margin-left'));
        
            $('.toggle-menu').addClass('icon-menu-online-close');
            $('.toggle-menu').removeClass('icon-toggle_tablet');

        }
        $('.mobile').toggleClass('active');
        $('.main-page').toggleClass('active');
        $('body').toggleClass('no-scroll');
    });

    $('.search').hover(function(){
        toggleTwoClasses($(this), 'icon-icon-search-big', 'icon-icon-search-big-white');
    });

    $('.to-top-wrapper').hover(function(){
        var div = $(this).find('div');
        if(div.hasClass('icon-scrollTop')){
            div.removeClass('icon-scrollTop').addClass('icon-scrollTop-white');
        }
        else if(div.hasClass('icon-scrollTop-white')) {
            div.removeClass('icon-scrollTop-white').addClass('icon-scrollTop');
        }
    });
