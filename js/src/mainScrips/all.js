$(".to-top-wrapper").click(function (event) {
  event.preventDefault();
  $('html, body').animate({
    scrollTop: 0
  }, 500);
});
//$('.horizontal-menu menu').flexMenu();
$('.horizontal-menu menu').flexMenu({
  showOnHover: false,
  linkText: "●●●",
  cutoff: 0,
  threshold: 1
});


$(".carousel").carousel({
  interval: false,
});

$('.news-slider').owlCarousel({
  // navigation : true, // Show next and prev buttons
  slideSpeed: 300,
  paginationSpeed: 400,
  singleItem: true
});

var elems = $(".carousel-inner div").length;
var elem = $(".carousel-inner div.active").index() + 1;
var first_elem = $(".slide_number").html(elem + " из " + elems);

$(".carousel-control").click(function () {
  if ($(this).hasClass("right")) {
    elem = $(".carousel-inner div.active").index() + 1;
    if (elem < elems) {
      $(".slide_number").html(elem + 1 + " из " + elems);
      elem++;
    } else {
      elem = 0;
      $(".slide_number").html(elem + 1 + " из " + elems);
    }
  } else {
    elem = $(".carousel-inner div.active").index();
    if (elem == 0) {
      elem = elems;
      $(".slide_number").html(elem + " из " + elems);
    } else {
      $(".slide_number").html(elem + " из " + elems);
    }
  }
});

/*$("a[href='#toptop']").click(function () {
  $("html, body").animate({
    scrollTop: 0
  }, 600);
  return false;
});*/